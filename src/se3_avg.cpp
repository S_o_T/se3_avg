#include "se3_avg/se3_avg.hpp"
#include <Eigen/StdVector>
#include <sophus/average.hpp>
#include <sophus/se3.hpp>

void se3_avg(const double el[], int n, double avg[], bool fitToSE3,
             int max_iterations) {
  std::vector<Sophus::SE3d, Eigen::aligned_allocator<Sophus::SE3d>> v;
  for (int i = 0; i < n; i++) {
    Eigen::Map<const Eigen::Matrix<double, 4, 4>> m(&el[i * 16]);
    if (fitToSE3) {
      v.emplace_back(Sophus::SE3d::fitToSE3(m));
    } else {
      v.emplace_back(m);
    }
  }
  if (auto result = Sophus::iterativeMean(v, max_iterations)) {
    memcpy(avg, result->matrix().data(), sizeof(double) * 16);
  } else {
    memcpy(avg, v[0].data(), sizeof(double) * 16);
  }
}
