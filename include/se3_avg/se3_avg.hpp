#ifndef SE3_AVG_SE3_AVG_HPP
#define SE3_AVG_SE3_AVG_HPP

// Calculate average of many se3 elements (rigid transformations)
// Assuming v containing n 4x4 matricies in ColumnMajor order
// Result is written at avg in ColumnMajor order
void se3_avg(const double v[], int n, double avg[], bool fitToSE3 = false,
             int max_iterations = 10);

#endif // SE3_AVG_SE3_AVG_HPP
