#include <Eigen/Core>
#include <iostream>
#include <se3_avg/se3_avg.hpp>
#include <sophus/se3.hpp>
#include <vector>

int main() {
  std::default_random_engine engine;
  auto orig = Sophus::SE3d::sampleUniform(engine);
  auto pert = Sophus::SE3d::rotX(M_PI / 8) * Sophus::SE3d::transY(1);
  auto a = (pert * orig).matrix();
  auto b = (pert.inverse() * orig).matrix();

  std::vector<Eigen::Matrix4d, Eigen::aligned_allocator<Eigen::Matrix4d>> v;
  v.emplace_back(a);
  v.emplace_back(b);

  std::vector<double> data(v.size() * 16);

  for (int i = 0; i < v.size(); i++) {
    for (int j = 0; j < 16; j++) {
      data[i * 16 + j] = v[i].data()[j];
    }
  }

  std::vector<double> ans(16);

  se3_avg(data.data(), v.size(), ans.data());

  Eigen::Map<Eigen::Matrix4d> avg(ans.data());
  std::cout << orig.matrix() << std::endl;
  std::cout << a << std::endl;
  std::cout << b << std::endl;
  std::cout << avg << std::endl;

  // proper se3 metric should be here, i guess
  std::cout << (orig.matrix() - avg).norm() << std::endl;
}